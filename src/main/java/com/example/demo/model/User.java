package com.example.demo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
//@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Entity
@Table(name="user")
public class User {
	@Id
	private int id;

	@Size(min = 5,max = 30)
	@NotNull
	private String name;

	@Size(min = 5,max = 30)
	private String address;

	@NotNull
	@Min(18)
	@Max(60)
	private int age;

	@NotBlank(message = "Position is mandatory")
	private String position;
	// 1- nu
	// 0 -nam
	private int sex;

	private Date dateOfBirth; 

	private double salary;
	@Size(min = 5,max = 30)
	private String company;

	private String status;

	private String token;

}
