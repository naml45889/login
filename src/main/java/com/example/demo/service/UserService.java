package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.data.UserRepository;
import com.example.demo.model.User;


@Service
public class UserService {

	@Autowired
	private UserRepository userRepo;
	
	public Iterable<User> getAllusers() {
		return userRepo.findAll();
	}
	public User userById(int id) {
		Optional<User> optuser = userRepo.findById(id);
		if (optuser.isPresent()) {
			return optuser.get();
		}
		return null;
	}
	public User createuser(User user) {
		return userRepo.save(user);
	}
	public User changeuser(User user) {
		return userRepo.save(user);
	}
	public void deleteuser(int id) {
		try {
			userRepo.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
	}
}
