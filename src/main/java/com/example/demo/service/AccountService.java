package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.data.AccountRepository;
import com.example.demo.model.Account;


@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepo;
	
	public List<Account> getAllaccounts() {
		return (List<Account>) accountRepo.findAll();
	}
	public Account accountByUserName(String username) {
		Optional<Account> optaccount = accountRepo.findByUsername(username);
		if (optaccount.isPresent()) {
			return optaccount.get();
		}
		return null;
	}
//	public Account createaccount(Account account) {
//		return accountRepo.save(account);
//	}
//	public Account changeaccount(Account account) {
//		return accountRepo.save(account);
//	}
//	public void deleteaccount(int id) {
//		try {
//			accountRepo.deleteById(id);
//		} catch (EmptyResultDataAccessException e) {
//			e.printStackTrace();
//		}
//	}
}
