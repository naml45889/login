package com.example.demo.controller;

import com.example.demo.model.Account;
import com.example.demo.model.User;
import com.example.demo.service.AccountService;
import com.example.demo.service.UserService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

@RestController
@RequestMapping(path = "/login", produces = "application/json")
@CrossOrigin(origins = "*")
//@Api(value = "API Description") // it description of api at top
public class LoginController {
	@Autowired
	private AccountService accountService;
	private UserService userService;
	public LoginController(AccountService accountService,UserService userService) {
		this.accountService = accountService;
		this.userService=userService;
	}
//	@ApiOperation(value = "It will get all account")
	@GetMapping
	public ResponseEntity<Iterable<Account>> getAllaccounts() {
		return ResponseEntity.ok(accountService.getAllaccounts());
	}
//	@ApiOperation(value = "It will get one account")
	@GetMapping("/{username}&{password}&{token1}")
	public ResponseEntity<Account> accountById(@PathVariable("username") String username,
			@PathVariable("password") String password,@PathVariable("token1") String token1) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		ArrayList<Account> accounts=(ArrayList<Account>) accountService.getAllaccounts();
		ArrayList<User> users= (ArrayList<User>) userService.getAllusers();
		System.out.println(accounts);
			token1=username+" "+password;
			String str=Base64.encode(token1.getBytes());
			for(int i=0;i<accounts.size();i++) {
				if(username.equals(accounts.get(i).getUsername())&&password.equals(accounts.get(i).getPassword())) {
					Account a=accounts.get(i);
					for(int j=0;j<users.size();j++) {
						if(users.get(j).getId()==a.getUser().getId()) {
							System.out.println("asasas");
							User u=new User();
							u.setSalary(users.get(j).getSalary());
							u.setToken(str);
							u.setId(users.get(j).getId());
							u.setAddress(users.get(j).getAddress());
							u.setAge(users.get(j).getAge());
							u.setCompany(users.get(j).getCompany());
							u.setDateOfBirth(users.get(j).getDateOfBirth());
							u.setName(users.get(j).getName());
							u.setPosition(users.get(j).getPosition());
							u.setSex(users.get(j).getSex());
							u.setStatus(users.get(j).getStatus());
							userService.changeuser(u);
						} 
					}
				}
			} 
		return new ResponseEntity<>(null,HttpStatus.CREATED);
	}
}