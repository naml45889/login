package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/user", produces = "application/json")
@CrossOrigin(origins = "*")
@Api(value = "API Description") // it description of api at top
public class UserController {
	@Autowired
	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}
	@ApiOperation(value = "It will get all user")
	@GetMapping
	public ResponseEntity<Iterable<User>> getAllusers() {
		return ResponseEntity.ok(userService.getAllusers());
	}
	@ApiOperation(value = "It will get one account")
	@GetMapping("/")
	public ResponseEntity<User> userById(@RequestParam("id") int id, @RequestParam("token") String token) {
		User u=userService.userById(id);
			if(token.equals(u.getToken())) {
				return ResponseEntity.ok(userService.userById(id));
			}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	@ApiOperation(value = "It will create one account")
	@PostMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<User> createuser(@Valid @RequestBody User user) {
		return ResponseEntity.ok(userService.createuser(user));
	}
	@ApiOperation(value = "It will change one account")
	@PutMapping("/{id}&{token}")
	public ResponseEntity<User> changeuser(@Valid @RequestBody User user) {
		return ResponseEntity.ok(userService.changeuser(user));
	}
	@ApiOperation(value = "It will delete one account")
	@Transactional
	@DeleteMapping("/{id}&{token}")
	public void deleteuser(@PathVariable("id") int id,@PathVariable("token") String token) {
		userService.deleteuser(id);
	}
}