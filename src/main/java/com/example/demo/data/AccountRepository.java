package com.example.demo.data;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Account;

public interface AccountRepository extends CrudRepository<Account, String> {
	Optional<Account> findByUsername(String username);
//	@Query("SELECT username,password FROM Account")
//	List<Account> findAllUsers();
}
