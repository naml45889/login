package com.example.demo;

import com.example.demo.controller.UserController;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.BasicJsonTester;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.Date;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@ExtendWith(SpringExtension.class)
//@WebMvcTest(controllers = UserController.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestInterceptorApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUserControllers {

    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    @Test
    public void verifyAllUserList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2))).andDo(print())
                .andExpect(jsonPath("$[*].id").exists())
                .andExpect(jsonPath("$[*].name").exists())
                .andExpect(jsonPath("$[*].address").exists())
                .andExpect(jsonPath("$[*].position").exists())
                .andExpect(jsonPath("$[*].sex").exists())
                .andExpect(jsonPath("$[*].dateOfBirth").exists())
                .andExpect(jsonPath("$[*].salary").exists())
                .andExpect(jsonPath("$[*].company").exists())
                .andExpect(jsonPath("$[*].status").exists())
                .andExpect(jsonPath("$[*].token").exists());
    }

    @Test
    public void verifyUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/?id={id}&token={token}", 1, "bmFtbGMgMTIzNA==")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("luu chan nam"))
                .andExpect(jsonPath("$.address").value("Ba Dinh, Ha Noi"))
                .andExpect(jsonPath("$.position").value("thuc tap sinh"))
                .andExpect(jsonPath("$.age").value(20))
                .andExpect(jsonPath("$.sex").value(0))
                .andExpect(jsonPath("$.dateOfBirth").value("1999-01-01T00:00:00.000+00:00"))
                .andExpect(jsonPath("$.salary").value(1000))
                .andExpect(jsonPath("$.company").value("ominext"))
                .andExpect(jsonPath("$.status").value("binh thuong"))
                .andExpect(jsonPath("$.token").value("bmFtbGMgMTIzNA=="));
    }

    @Test
    public void cannotVerifyUserByToken() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/user/?id={id}&token={token}", 1, "bmFtbGMgMTIzNA")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void createUser() throws Exception {
        User user = new User();
        user.setName("test1111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(2);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.name").value("test1111111"))
                .andExpect(jsonPath("$.address").value("test111111"))
                .andExpect(jsonPath("$.position").value("test11"))
                .andExpect(jsonPath("$.age").value(21))
                .andExpect(jsonPath("$.sex").value(1))
                .andExpect(jsonPath("$.dateOfBirth").exists())
                .andExpect(jsonPath("$.salary").value(1111))
                .andExpect(jsonPath("$.company").value("test11111"))
                .andExpect(jsonPath("$.status").value("test"))
                .andExpect(jsonPath("$.token").value("test"));
    }

    @Test
    public void cannotCreateUserByName() throws Exception {
        User user = new User();
        user.setName("test");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(2);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotCreateUserByAddress() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotCreateUserByAge() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(12);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotCreateUserByCompany() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotCreateUserByPosition() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateUser() throws Exception {
        User user = new User();
        user.setName("testput");
        user.setToken("testput");
        user.setSex(1);
        user.setStatus("testput");
        user.setSalary(1111);
        user.setPosition("testput");
        user.setAge(21);
        user.setCompany("testput");
        user.setId(2);
        user.setAddress("testput");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.name").value("testput"))
                .andExpect(jsonPath("$.address").value("testput"))
                .andExpect(jsonPath("$.position").value("testput"))
                .andExpect(jsonPath("$.age").value(21))
                .andExpect(jsonPath("$.sex").value(1))
                .andExpect(jsonPath("$.dateOfBirth").exists())
                .andExpect(jsonPath("$.salary").value(1111))
                .andExpect(jsonPath("$.company").value("testput"))
                .andExpect(jsonPath("$.status").value("testput"))
                .andExpect(jsonPath("$.token").value("testput"));
    }

    @Test
    public void cannotUpdateUserByName() throws Exception {
        User user = new User();
        user.setName("test");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(2);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotUpdateUserByAddress() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotUpdateUserByAge() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(12);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotUpdateUserByCompany() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setPosition("test11");
        user.setAge(21);
        user.setCompany("test");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void cannotUpdateUserByPosition() throws Exception {
        User user = new User();
        user.setName("test111111");
        user.setToken("test");
        user.setSex(1);
        user.setStatus("test");
        user.setSalary(1111);
        user.setAge(21);
        user.setCompany("test11111");
        user.setId(3);
        user.setAddress("test111111");
        user.setDateOfBirth(new Date());
        mockMvc.perform(MockMvcRequestBuilders
                .put("/user/2&testtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void verifyDeleteUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/2&testput").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
