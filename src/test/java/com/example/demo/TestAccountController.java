package com.example.demo;

import com.example.demo.service.AccountService;
import com.example.demo.service.UserService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestInterceptorApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAccountController {

    private MockMvc mockMvc;

    @Autowired
    private AccountService accountService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    @Test
    public void verifyAllAccountList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/login")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1))).andDo(print())
                .andExpect(jsonPath("$[*].id").exists())
                .andExpect(jsonPath("$[*].username").exists())
                .andExpect(jsonPath("$[*].password").exists())
                .andExpect(jsonPath("$[*].user.id").exists())
                .andExpect(jsonPath("$[*].user.name").exists())
                .andExpect(jsonPath("$[*].user.address").exists())
                .andExpect(jsonPath("$[*].user.position").exists())
                .andExpect(jsonPath("$[*].user.sex").exists())
                .andExpect(jsonPath("$[*].user.dateOfBirth").exists())
                .andExpect(jsonPath("$[*].user.salary").exists())
                .andExpect(jsonPath("$[*].user.company").exists())
                .andExpect(jsonPath("$[*].user.status").exists())
                .andExpect(jsonPath("$[*].user.token").exists());
    }

    @Test
    public void verifyAccountById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/login/namlc&1234&1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }
}
