package com.example.demo.service;

import com.example.demo.data.UserRepository;
import com.example.demo.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)

public class UserServiceTest {
    @Autowired
    MockMvc mockMvc;

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserService userService;

    List<User> users = new ArrayList<User>();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        User u=new User();
        u.setId(1);
        u.setName("luu chan nam");
        u.setToken("test1234");
        u.setStatus("binh thuong");
        u.setSex(1);
        u.setSalary(1000);
        u.setPosition("thuc tap sinh");
        u.setAge(20);
        u.setCompany("ominext");
        u.setAddress("Ba Dinh, Ha Noi");
        users.add(u);
    }
    @Test
    public void testGetAllUser() {
        when(userRepository.findAll()).thenReturn(users);
        List<User> result= (List<User>) userService.getAllusers();
        assertEquals(1, result.size());
    }
    @Test
    public void testGetUserById() {
        User u = new User();
        u.setId(1);
        u.setName("luu chan nam");
        u.setToken("test1234");
        u.setStatus("binh thuong");
        u.setSex(1);
        u.setSalary(1000);
        u.setPosition("thuc tap sinh");
        u.setAge(20);
        u.setCompany("ominext");
        u.setAddress("Ba Dinh, Ha Noi");
        when(userRepository.findById(1)).thenReturn(Optional.of(u));
        Optional<User> resultOpt = Optional.ofNullable(userService.userById(1));
        User result = resultOpt.get();
        assertEquals(1, result.getId());
        assertEquals("luu chan nam", result.getName());
        assertEquals("test1234", result.getToken());

    }

    @Test
    public void testPostUser() {
        User u = new User();
        u.setId(1);
        u.setName("luu chan nam");
        u.setToken("test1234");
        u.setStatus("binh thuong");
        u.setSex(1);
        u.setSalary(1000);
        u.setPosition("thuc tap sinh");
        u.setAge(20);
        u.setCompany("ominext");
        u.setAddress("Ba Dinh, Ha Noi");

        userService.createuser(u);
        verify(userRepository, times(1)).save(any());
    }

}
